﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow2D : MonoBehaviour {

	public float dampTime = 0.15f; // Creates a delay when camera follows character
	private Vector3 velocity = Vector3.zero; // Allows camera to follow characters different positions
	public Transform target; // position of game object

	public bool bounds; // Boolean: True or false 

	public Vector3 minCameraPos;
	public Vector3 maxCameraPos; // Minimum and Maximum Camera Position

	
	// Update is called once per frame
	void Update () {
		if (target) {
			Vector3 point = GetComponent<Camera> ().WorldToViewportPoint (target.position); //Point of camera following a target
			Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, .50f, point.z)); //allows camera to hone in on character controller
			Vector3 destination = transform.position + delta; // transform position of character during passage of time
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime); // Lets camera follow character in a smooth way

			if (bounds) {
				transform.position = new Vector3(Mathf.Clamp (transform.position.x, minCameraPos.x, maxCameraPos.x),
					Mathf.Clamp (transform.position.y, minCameraPos.y, maxCameraPos.y),
					Mathf.Clamp (transform.position.z, minCameraPos.z, maxCameraPos.z));

		}
	}
}

}
